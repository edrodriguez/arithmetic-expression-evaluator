/**
 * @author Edward Rodriguez
 * PostfixStack processes each token from left to right in the arithmetic expression (String array)
 * and performs the appropriate action depending on the token (push/pop/print)
 * Postfix stack also stores equivalent postfix expression onto a queue
 */
public class PostfixStack {
	
	public static String LEFTPARENTHESIS = "(";
	public static String RIGHTPARENTHESIS = ")";
	public static String REGEX = "([-�+*/()])";
	public static String REGEXLOWPRECEDENCE = "([-�+])";
	public static String REGEXHIGHPRECEDENCE = "([*/])";
	public static String REGEXNOPARENTHESIS = "([-�+*/])";
	public static String SPACE = " ";
	
	//stack to store operators/parentheses
	private DynamicArrayStack<String> stack;
	private String[] arrayToProcess;
	private LinkedQueue<String> postfixExpressionQueue;
	
	//Default constructor initializing stack and queue
	 public PostfixStack(){
		stack = new DynamicArrayStack<>();
		postfixExpressionQueue = new LinkedQueue<>();
	}
	
	/**
	 * Constructor initializing stack/length & queue
	 * also clones string to process (arithmetic expression)
	 * @param s string to  process
	 */
	public PostfixStack(String[] s){
		postfixExpressionQueue = new LinkedQueue<>();
		stack = new DynamicArrayStack<>(s.length);
		arrayToProcess = s.clone();	
	}
	
	public DynamicArrayStack<String> getStack() {
		return stack;
	}

	public void setStack(DynamicArrayStack<String> stack) {
		this.stack = stack;
	}
	
	public LinkedQueue<String> getQueue() {
		return postfixExpressionQueue;
	}
	
	/**
	 * loops through each element of the string array to be processed
	 * and passes it to another method to determine the appropriate
	 * action for that element/token
	 * After the last token in the arithmetic expression is read and processed, 
	 * the remaining operators on the stack are popped and stored onto the queue
	 */
	public void convertToPostfix(){
		for (int i = 0; i < arrayToProcess.length; i++) {
			processToken(arrayToProcess[i]);
		}
		while (!stack.isEmpty()){
			String temp2 = stack.pop();
			postfixExpressionQueue.enqueue(temp2);
		}
	}

	/**
	 * processes substring and performs the appropriate action 
	 * depending on the token (push/pop/store onto queue)
	 * @param s substring to be processed
	 */
	private void processToken (String s){
		if(stack.isEmpty() && s.matches(REGEX)){
			stack.push(s);
		}
		
		else if (!stack.isEmpty() && s.matches(REGEX)){
			while(!stack.isEmpty()){
				if (s.matches(REGEXLOWPRECEDENCE) && stack.top().matches(REGEXNOPARENTHESIS)){
					postfixExpressionQueue.enqueue(stack.top());
					stack.pop();
				}
				else if (s.matches(REGEXHIGHPRECEDENCE) && stack.top().matches(REGEXHIGHPRECEDENCE)){
					postfixExpressionQueue.enqueue(stack.top());
					stack.pop();
				}
				else if (s.matches(REGEXHIGHPRECEDENCE) && stack.top().matches(REGEXLOWPRECEDENCE)){
					break;
				}
				else if (s.equals(LEFTPARENTHESIS)){
					break;
				}
				else if (s.equals(RIGHTPARENTHESIS)){
					while (!stack.top().equals(LEFTPARENTHESIS)){
						postfixExpressionQueue.enqueue(stack.top());
						stack.pop();
					}
					if (stack.top().equals(LEFTPARENTHESIS)){
						stack.pop();
						break;
					}
				}
				else break;
			}
			if (!s.equals(RIGHTPARENTHESIS)){
				stack.push(s);
			}
		}
		else postfixExpressionQueue.enqueue(s);	
	}
}

import java.util.Iterator;
import java.util.Scanner;

/**
 * @author Edward Rodriguez
 * ExpressionTree converts the postfix expression in the queue and creates the 
 * expression within a single Binary Tree to create the expression�s arithmetic 
 * expression tree.ExpressionTree also contains methods to evaluate and print the 
 * mathematical expression.
 */
public class ExpressionTree {
	
	public static final int DEFAULT_CAPACITY = 256;
	private LinkedStack<BinaryTree<String>> stack;
	private LinkedQueue<String> queue;
	private BinaryTree<String> tree;
	public static String REGEXOPERATORS = "([-�+*/])";
	public static String REGEXALPPHANUMERIC = "([a-zA-Z0-9]+)";
	public static String REGEXALPPHABET = "([a-zA-Z]+)";
	public static String REGEXSINGLELETTER = "([a-zA-Z])";
	public static String REGEXNUMERIC = "([0-9]+)";
	public static String PLUS = "+";
	public static String MINUS = "-";
	public static String SUBTRACT = "�";
	public static String MULTIPLY = "*";
	public static String DIVIDE = "/";
	public static String SPACE = " ";
	
	/**
	 * Constructor that takes a queue as a parameter and initializes
	 * stack, tree and sets queue from parameter
	 * @param postExpressionQueue queue to convert
	 */
	public ExpressionTree(LinkedQueue<String> postExpressionQueue) {
		stack = new LinkedStack<BinaryTree<String>>();
		queue = new LinkedQueue<String>();
		tree = new BinaryTree<String>();
		setQueue(postExpressionQueue);
	}
	
	public LinkedStack<BinaryTree<String>> getStack() {
		return stack;
	}
	public void setStack(LinkedStack<BinaryTree<String>> stack) {
		this.stack = stack;
	}
	public LinkedQueue<String> getQueue() {
		return queue;
	}
	public void setQueue(LinkedQueue<String> queue) {
		this.queue = queue;
	}
	public BinaryTree<String> getTree() {
		return tree;
	}
	public void setTree(BinaryTree<String> tree) {
		this.tree = tree;
	}
	
/**
 * method dequeue's each element from the queue and processes
 * each dequeued element. Operands (REGEXALPPHANUMERIC) are stored 
 * in a binary tree and pushed onto a stack of type BinaryTree. 
 * Operators are stored in a binary tree as root w/ left right children popped
 * from the stack and attached to operator tree. Once queue is empty, one 
 * element remains on the stack, then popped resulting in a 
 * arithmetic expression tree for the entire arithmetic expression 
 */
public void convertQueueToTree(){
		
		while (!queue.isEmpty()){
			String temp = queue.dequeue();
			if (temp.matches(REGEXALPPHANUMERIC)){
				BinaryTree<String> T = new BinaryTree<>(temp);
				stack.push(T);	
			}
			else{
				BinaryTree<String> T = new BinaryTree<>(temp);
				BinaryTree<String> rightChild = stack.pop();
				BinaryTree<String> leftChild = stack.pop();
				T.attach(leftChild, rightChild);
				stack.push(T);
			}
		}
		tree = stack.pop();	
	}

/**
 * method iterates through arithmetic expression tree processing each
 * element. Integers are stored onto a stack. Variables are stored onto the 
 * stack (after asking the user to input a number for the variable) and into an
 * array to keep track of recurring variables. Operators pop two numbers 
 * off the stack and perform the given operation. Result of operation is
 * pushed on top the stack.
 * @return result of operation(s)
 */
public float evaluateExpression(){
	int distinctVariables = 1;
	LinkedStack<Float> linkStack = new LinkedStack<>();
	Variable[] variableArrayHistory = new Variable[20];
	Iterator<String> itr = tree.iterator();
	float temp = 0;
	
	while(itr.hasNext()){
		String s = itr.next();
		if (s.matches(REGEXNUMERIC)){
			temp =  Integer.parseInt(String.valueOf(s));
			linkStack.push(temp);
		}
		else if (s.matches(REGEXSINGLELETTER)){
			Variable tempVar = evaluateVariable(s, distinctVariables, variableArrayHistory);
			linkStack.push(tempVar.getValue());
			variableArrayHistory[distinctVariables - 1] = tempVar;
			distinctVariables++;
		}
		else if (s.matches(REGEXALPPHANUMERIC)){
			float total = 1;
			for (int index = 0; index < s.length();index++) {
				char aChar = s.charAt(index);
				String substring = aChar + "";
				if (substring.matches(REGEXSINGLELETTER)){
				Variable tempVar = evaluateVariable(substring, distinctVariables, variableArrayHistory);
				variableArrayHistory[distinctVariables - 1] = tempVar;
				distinctVariables++;
				total = total * tempVar.getValue();
				}
				else{
					temp = Character.getNumericValue(aChar);
					total = total * temp;
				}
			}
			linkStack.push(total);
		}
		else {
			float result;
			float right = linkStack.pop();
			float left = linkStack.pop();
			if (s.equals(PLUS)) result = left + right;
			else if (s.equals(MINUS) || s.equals(SUBTRACT)) result = left - right;
			else if (s.equals(DIVIDE)) result = left / right;
			else  result = left * right;
			
			linkStack.push(result);	
		}
	}
	return linkStack.pop();
}

/**
 * method that evaluates each variable, asks user for input if 
 * variable has not yet been seen and stores variable with value 
 * in an array of type Variable.
 * @param s string to process
 * @param n number of variables seen
 * @param src array w/ variables already seen
 * @return
 */
private Variable evaluateVariable(String s, int n, Variable[] src){
	Variable[] dest = new Variable[src.length];
	System.arraycopy( src, 0, dest, 0, src.length );
	Variable tempVar = new Variable(s, 0);
	float variable = 0;
	
	for (int i = 0; i < n; i++){
		if (dest[i] != null && tempVar.compareTo(dest[i]) == 1){
			tempVar.setValue(dest[i].getValue());
			return tempVar;
		}		
	}
	Scanner reader = new Scanner(System.in);  // Reading from System.in
	System.out.print("Enter a number for the variable '" + s + "' :");
	variable = reader.nextFloat(); // Scans the next token of the input as an int.;
	tempVar.setValue(variable);
	dest[n - 1] = tempVar;
	src = dest;
	return tempVar;
}

/**
 * Iterates  through  the  arithmetic  expression  tree,  printing  each 
 * element returned by a call to the next() method of the iterator.
 */
public void print(){
	Iterator<String> itr = this.tree.iterator();
	System.out.print("Postfix: ");
	while (itr.hasNext()){
		System.out.print(itr.next() + SPACE);
	}
	System.out.print("\n");
}

}

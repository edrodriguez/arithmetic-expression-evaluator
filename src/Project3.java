import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Edward Rodriguez
 * Date: 12/07/16
 * 
 * Program that reads one or more arithmetic expressions written in infix 
 * notation from a text file then converts and prints the equivalent arithmetic 
 * postfix expression. The expression is then evaluated (with user input) and 
 * result is printed.
 */
public class Project3 {

	public static String FILE = "project3.txt";
	public static String SPACE = "\\s+";
	public static String REGEX = "(?<=[-�+*/()])|(?=[-�+*/()])";
	
	/**
	 * Main method opens the file "project3.txt" then reads 1 line at a time until 
	 * the end of the file is reached. Each line (arithmetic expression) is split 
	 * into substrings around matches of the regular expression (operators & 
	 * parentheses). The substrings are then stored in an array of type String and 
	 * passed to the PostfixStack class to convert to postfix (stack and queue). 
	 * Resulting postfix expression(queue) is passed onto ExpressionTree class to 
	 * convert expression into a single BinaryTree. The postfix expression is then 
	 * printed and evaluated via the ExpressionTree class.
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		FileInputStream fstream = new FileInputStream(FILE);
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(fstream))){
			
			String [] lineArray ={};
			String line;
	
			while ((line = reader.readLine()) != null){
				if (!line.isEmpty()) {
					System.out.println("Infix expression: " + line);
					line = line.replaceAll(SPACE,""); 
					lineArray = line.split(REGEX);
					PostfixStack q1 = new PostfixStack(lineArray);
					q1.convertToPostfix();
					
					ExpressionTree e1 = new ExpressionTree(q1.getQueue());
					e1.convertQueueToTree();
					e1.print();
					System.out.println("answer: " + e1.evaluateExpression());
				}
				System.out.print("\n");
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();	
		  }	
	}
}

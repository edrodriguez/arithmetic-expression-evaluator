
/**
 * @author Edward Rodriguez
 * Variable stores the variable of the expression and 
 * the value of the variable
 */
public class Variable implements Comparable<Variable>{
	
	private String var;
	private float value;
	
	//default constructor
	public Variable() {
		var = null;
		value = 0;
	}
	
	public Variable(String variable, float val) {
		var = variable;
		value = val;
	}
	
	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */

	@Override
	public int compareTo(Variable o) {
		if (this.var.equals(o.getVar()))
			return 1;
		
		return -1;
	}

	

}
